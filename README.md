# Sync Releases

This is a Gitea Action to sync releases from a Github/Gitea repository to another Gitea repository on the same site or different site.

It can also be used as a commmand line tool.

## Usage

```shell
./sync_gh_gitea_releases --source_type=github --source_repo_url="https://github.com/go-xorm/dbweb" --source_repo_token=xxxxxxxx --gitea_repo_url="http://localhost:3000/lunny2/test-sync-github-releases2" --gitea_repo_token=xxxxxxx
```
