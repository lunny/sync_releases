package main

import (
	"context"
	"log"
	"os"

	"github.com/urfave/cli/v2"

	gha "github.com/sethvargo/go-githubactions"

	"gitea.com/lunny/sync_releases/internal/pkg/sync"
)

func main() {
	app := &cli.App{
		Name:  "sync_releases",
		Usage: "sync github/gitea releases to gitea",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:  "source_repo_type",
				Value: "",
				Usage: "Source repository type, github or gitea",
			},
			&cli.StringFlag{
				Name:  "source_repo_url",
				Value: "",
				Usage: "source repository url",
			},
			&cli.StringFlag{
				Name:  "source_repo_token",
				Value: "",
				Usage: "Source repository token",
			},
			&cli.StringFlag{
				Name:  "gitea_repo_url",
				Value: "",
				Usage: "Gitea repository url",
			},
			&cli.StringFlag{
				Name:  "gitea_repo_token",
				Value: "",
				Usage: "Gitea repository url",
			},
		},
		Action: cmd,
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

func cmd(cli *cli.Context) error {
	fromType, fromRepo, fromToken := cli.String("source_repo_type"), cli.String("source_repo_url"), cli.String("source_repo_token")
	toRepo, toToken := cli.String("gitea_repo_url"), cli.String("gitea_repo_token")

	if fromRepo == "" {
		fromType = gha.GetInput("source_repo_type")
		fromRepo = gha.GetInput("source_repo_url")
		fromToken = gha.GetInput("source_repo_token")
		toRepo = gha.GetInput("gitea_repo_url")
		toToken = gha.GetInput("gitea_repo_token")
	}

	return sync.Sync(context.Background(), fromType, fromRepo, fromToken, toRepo, toToken)
}
