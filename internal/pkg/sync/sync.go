package sync

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"sort"
	"strings"

	"code.gitea.io/sdk/gitea"
	"github.com/google/go-github/v56/github"
)

type Result struct {
	Status  bool
	Version string
	Reason  string
}

func getGithubReleases(githubClient *github.Client, githubOwner, githubRepo string) ([]*github.RepositoryRelease, error) {
	var releases []*github.RepositoryRelease

	for i := 0; ; i++ {
		rels, _, err := githubClient.Repositories.ListReleases(context.Background(), githubOwner, githubRepo, &github.ListOptions{
			Page:    i,
			PerPage: 100,
		})
		if err != nil {
			return nil, err
		}
		if len(rels) == 0 {
			break
		}
		releases = append(releases, rels...)
	}

	sort.Slice(releases, func(i, j int) bool {
		return releases[i].CreatedAt.Before(releases[j].CreatedAt.Time)
	})
	return releases, nil
}

func getGiteaReleases(giteaClient *gitea.Client, giteaOwner, giteaRepo string) ([]*gitea.Release, error) {
	var releases []*gitea.Release
	for i := 0; ; i++ {
		rels, _, err := giteaClient.ListReleases(giteaOwner, giteaRepo, gitea.ListReleasesOptions{
			ListOptions: gitea.ListOptions{
				Page:     i,
				PageSize: 100,
			},
		})
		if err != nil {
			return nil, err
		}
		if len(rels) == 0 {
			break
		}
		releases = append(releases, rels...)
	}

	sort.Slice(releases, func(i, j int) bool {
		return releases[i].CreatedAt.Before(releases[j].CreatedAt)
	})
	return releases, nil
}

func createGiteaReleaseFromGithubRelease(ctx context.Context, rel *github.RepositoryRelease, giteaClient *gitea.Client, giteaOwner, giteaRepo string) (*gitea.Release, error) {
	fmt.Printf("creating release %s\n", rel.GetName())
	giteaRel, _, err := giteaClient.CreateRelease(giteaOwner, giteaRepo, gitea.CreateReleaseOption{
		TagName: rel.GetTagName(),
		Target:  rel.GetTargetCommitish(),
		Title:   rel.GetName(),
		Note:    rel.GetBody(),
	})
	if err != nil {
		return nil, err
	}

	return giteaRel, nil
}

func createGiteaReleaseFromGiteaRelease(ctx context.Context, rel *gitea.Release, giteaClient *gitea.Client, giteaOwner, giteaRepo string) (*gitea.Release, error) {
	fmt.Printf("creating release %s\n", rel.Title)
	giteaRel, _, err := giteaClient.CreateRelease(giteaOwner, giteaRepo, gitea.CreateReleaseOption{
		TagName: rel.TagName,
		Target:  rel.Target,
		Title:   rel.Title,
		Note:    rel.Note,
	})
	if err != nil {
		return nil, err
	}

	return giteaRel, nil
}

func createReleaseAttachmentsFromGithubRelease(ctx context.Context, rel *github.RepositoryRelease, giteaRel *gitea.Release, giteaClient *gitea.Client, giteaOwner, giteaRepo string) error {
	for _, att := range rel.Assets {
		fmt.Printf("creating attachment %s\n", att.GetName())
		resp, err := http.Get(*att.BrowserDownloadURL)
		if err != nil {
			return err
		}
		if _, _, err := giteaClient.CreateReleaseAttachment(giteaOwner, giteaRepo, giteaRel.ID, resp.Body, att.GetName()); err != nil {
			resp.Body.Close()
			return err
		}
		resp.Body.Close()
	}
	return nil
}

func createReleaseAttachmentsFromGiteaRelease(ctx context.Context, rel *gitea.Release, giteaRel *gitea.Release, giteaClient *gitea.Client, giteaOwner, giteaRepo string) error {
	for _, att := range rel.Attachments {
		fmt.Printf("creating attachment %s\n", att.Name)
		resp, err := http.Get(att.DownloadURL)
		if err != nil {
			return err
		}
		if _, _, err := giteaClient.CreateReleaseAttachment(giteaOwner, giteaRepo, giteaRel.ID, resp.Body, att.Name); err != nil {
			resp.Body.Close()
			return err
		}
		resp.Body.Close()
	}
	return nil
}

func syncReleaseAttachmentsFromGithubRelease(ctx context.Context, rel *github.RepositoryRelease, giteaRel *gitea.Release, giteaClient *gitea.Client, giteaOwner, giteaRepo string) error {
	fmt.Printf("sync release %s attachments\n", rel.GetName())
	for _, att := range rel.Assets {
		resp, err := http.Get(*att.BrowserDownloadURL)
		if err != nil {
			return err
		}

		found := false
		for _, giteaAtt := range giteaRel.Attachments {
			if giteaAtt.Name == att.GetName() {
				found = true
				break
			}
		}
		if found {
			continue
		}
		fmt.Println("create attachment", att.GetName())
		if _, _, err := giteaClient.CreateReleaseAttachment(giteaOwner, giteaRepo, giteaRel.ID, resp.Body, att.GetName()); err != nil {
			resp.Body.Close()
			return err
		}
		resp.Body.Close()
	}
	return nil
}

func syncReleaseAttachmentsFromGiteaRelease(ctx context.Context, rel *gitea.Release, giteaRel *gitea.Release, giteaClient *gitea.Client, giteaOwner, giteaRepo string) error {
	fmt.Printf("sync release %s attachments\n", rel.Title)
	for _, att := range rel.Attachments {
		resp, err := http.Get(att.DownloadURL)
		if err != nil {
			return err
		}

		found := false
		for _, giteaAtt := range giteaRel.Attachments {
			if giteaAtt.Name == att.Name {
				found = true
				break
			}
		}
		if found {
			continue
		}
		fmt.Println("create attachment", att.Name)
		if _, _, err := giteaClient.CreateReleaseAttachment(giteaOwner, giteaRepo, giteaRel.ID, resp.Body, att.Name); err != nil {
			resp.Body.Close()
			return err
		}
		resp.Body.Close()
	}
	return nil
}

func syncGithubReleases(ctx context.Context, giteaClient *gitea.Client, githubReleases []*github.RepositoryRelease, giteaOwner, giteaRepo string, giteaReleases []*gitea.Release) error {
	for _, rel := range githubReleases {
		var giteaRel *gitea.Release
		for _, iRel := range giteaReleases {
			if rel.GetName() == iRel.Title {
				giteaRel = iRel
				break
			}
		}

		if giteaRel == nil {
			giteaRel, err := createGiteaReleaseFromGithubRelease(ctx, rel, giteaClient, giteaOwner, giteaRepo)
			if err == nil {
				err = createReleaseAttachmentsFromGithubRelease(ctx, rel, giteaRel, giteaClient, giteaOwner, giteaRepo)
			}

			if err != nil {
				return err
			}
		} else {
			err := syncReleaseAttachmentsFromGithubRelease(ctx, rel, giteaRel, giteaClient, giteaOwner, giteaRepo)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func syncGiteaReleases(ctx context.Context, toClient *gitea.Client, fromReleases []*gitea.Release, toOwner, toRepo string, toReleases []*gitea.Release) error {
	for _, rel := range fromReleases {
		var giteaRel *gitea.Release
		for _, iRel := range toReleases {
			if rel.Title == iRel.Title {
				giteaRel = iRel
				break
			}
		}

		if giteaRel == nil {
			giteaRel, err := createGiteaReleaseFromGiteaRelease(ctx, rel, toClient, toOwner, toRepo)
			if err == nil {
				err = createReleaseAttachmentsFromGiteaRelease(ctx, rel, giteaRel, toClient, toOwner, toRepo)
			}

			if err != nil {
				return err
			}
		} else {
			err := syncReleaseAttachmentsFromGiteaRelease(ctx, rel, giteaRel, toClient, toOwner, toRepo)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func Sync(ctx context.Context, fromType, fromRepo, fromToken, toRepo, toToken string) error {
	switch strings.ToLower(fromType) {
	case "github":
		return syncGithub(ctx, fromRepo, fromToken, toRepo, toToken)
	case "gitea":
		return syncGitea(ctx, fromRepo, fromToken, toRepo, toToken)
	default:
		return fmt.Errorf("invalid source repo type: %v", fromType)
	}
}

func syncGithub(ctx context.Context, fromRepo, fromToken, toRepo, toToken string) error {
	fromURL, err := url.Parse(fromRepo)
	if err != nil {
		return err
	}
	parties := strings.Split(strings.TrimPrefix(fromURL.Path, "/"), "/")
	if len(parties) != 2 {
		return fmt.Errorf("invalid github repo url: %v", toRepo)
	}
	githubOwner := parties[0]
	githubRepo := strings.TrimSuffix(parties[1], ".git")

	githubReleases, err := getGithubReleases(github.NewTokenClient(ctx, fromToken), githubOwner, githubRepo)
	if err != nil {
		return err
	}

	toURL, err := url.Parse(toRepo)
	if err != nil {
		return err
	}
	giteaBaseUrl := toURL.Scheme + "://" + toURL.Host
	parties = strings.Split(strings.TrimPrefix(toURL.Path, "/"), "/")
	if len(parties) != 2 {
		return fmt.Errorf("invalid gitea repo url: %v", toRepo)
	}

	giteaOwner := parties[0]
	giteaRepo := strings.TrimSuffix(parties[1], ".git")
	giteaClient, err := gitea.NewClient(giteaBaseUrl, gitea.SetToken(toToken))
	if err != nil {
		return err
	}

	giteaReleases, err := getGiteaReleases(giteaClient, giteaOwner, giteaRepo)
	if err != nil {
		return err
	}

	return syncGithubReleases(ctx, giteaClient, githubReleases, giteaOwner, giteaRepo, giteaReleases)
}

func syncGitea(ctx context.Context, fromRepo, fromToken, toRepo, toToken string) error {
	fromURL, err := url.Parse(fromRepo)
	if err != nil {
		return err
	}
	fromBaseUrl := fromURL.Scheme + "://" + fromURL.Host

	parties := strings.Split(strings.TrimPrefix(fromURL.Path, "/"), "/")
	if len(parties) != 2 {
		return fmt.Errorf("invalid github repo url: %v", toRepo)
	}
	fromRepoOwner := parties[0]
	fromRepoName := strings.TrimSuffix(parties[1], ".git")

	fromClient, err := gitea.NewClient(fromBaseUrl, gitea.SetToken(fromToken))
	if err != nil {
		return err
	}

	fromReleases, err := getGiteaReleases(fromClient, fromRepoOwner, fromRepoName)
	if err != nil {
		return err
	}

	toURL, err := url.Parse(toRepo)
	if err != nil {
		return err
	}
	giteaBaseUrl := toURL.Scheme + "://" + toURL.Host
	parties = strings.Split(strings.TrimPrefix(toURL.Path, "/"), "/")
	if len(parties) != 2 {
		return fmt.Errorf("invalid gitea repo url: %v", toRepo)
	}

	giteaOwner := parties[0]
	giteaRepo := strings.TrimSuffix(parties[1], ".git")
	giteaClient, err := gitea.NewClient(giteaBaseUrl, gitea.SetToken(toToken))
	if err != nil {
		return err
	}

	giteaReleases, err := getGiteaReleases(giteaClient, giteaOwner, giteaRepo)
	if err != nil {
		return err
	}

	return syncGiteaReleases(ctx, giteaClient, fromReleases, giteaOwner, giteaRepo, giteaReleases)
}
