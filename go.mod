module gitea.com/lunny/sync_releases

go 1.20

require (
	code.gitea.io/sdk/gitea v0.16.0
	github.com/google/go-github/v56 v56.0.0
	github.com/sethvargo/go-githubactions v1.1.0
	github.com/urfave/cli/v2 v2.25.7
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.2 // indirect
	github.com/davidmz/go-pageant v1.0.2 // indirect
	github.com/go-fed/httpsig v1.1.0 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/hashicorp/go-version v1.5.0 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/sethvargo/go-envconfig v0.8.0 // indirect
	github.com/xrash/smetrics v0.0.0-20201216005158-039620a65673 // indirect
	golang.org/x/crypto v0.7.0 // indirect
	golang.org/x/sys v0.8.0 // indirect
)
